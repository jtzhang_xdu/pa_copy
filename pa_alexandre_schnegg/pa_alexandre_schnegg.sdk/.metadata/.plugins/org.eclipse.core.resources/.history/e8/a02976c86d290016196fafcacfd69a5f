#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include "dma_proxy.h"
#include <stdlib.h>
#include <string.h>

#define BILLION 1E9

static struct dma_proxy_channel_interface *tx_proxy_interface_p;
static int tx_proxy_fd;

typedef unsigned int uint32_t;

typedef struct
{
	uint32_t ctrl;
	uint32_t status;
	uint32_t key[4];
} AesIp_t;

/* The following function is the transmit thread to allow the transmit and the
 * receive channels to be operating simultaneously. The ioctl calls are blocking
 * such that a thread is needed.
 */
void *tx_thread()
{
	unsigned char plaintext[16];
	int dummy, i;

	/* Perform the DMA transfer and the check the status after it completes
 	 * as the call blocks til the transfer is done.
 	 */
	ioctl(tx_proxy_fd, 0, &dummy);

	if (tx_proxy_interface_p->status != PROXY_NO_ERROR)
		printf("Proxy tx transfer error\n");
}

/* The following function uses the dma proxy device driver to perform DMA transfers
 * from user space. This app and the driver are tested with a system containing an
 * AXI DMA without scatter gather and with transmit looped back to receive.
 */
int main(int argc, char *argv[])
{
	struct dma_proxy_channel_interface *rx_proxy_interface_p;
	int rx_proxy_fd,aes_fd;
	FILE *input_fd,*output_fd;
	int dummy;
	pthread_t tid;
	long lSize,i,transferLength;
	struct timespec requestStart, requestEnd;
	double accum;
	int paddingSize;

	unsigned char ciphertext[16];
	unsigned char key[16];
	AesIp_t *aesIp;
	size_t result;

	//Parse args
	if(argc!=4)
	{
		printf( "usage: %s inputfile outputfile key\n", argv[0] );
		exit(EXIT_FAILURE);
	}

	printf("DMA proxy test\n");

	//Map the AES IP registers

	aes_fd = open("/dev/mem", O_RDWR|O_SYNC);
	if (aes_fd < 0)
	{
		perror("Error opening file for writing");
		exit(EXIT_FAILURE);
	}

	aesIp = (AesIp_t*) mmap(NULL, sizeof(AesIp_t), PROT_READ | PROT_WRITE, MAP_SHARED, aes_fd,
			0x43c00000);
	if (aesIp == MAP_FAILED)
	{
		close(aes_fd);
		perror("Error mmapping the file");
		exit(EXIT_FAILURE);
	}

	//Reset
	aesIp->ctrl=0x0;

	sscanf(argv[3], "%8x", &(aesIp->key[3]));
	sscanf(argv[3]+8, "%8x", &(aesIp->key[2]));
	sscanf(argv[3]+16, "%8x", &(aesIp->key[1]));
	sscanf(argv[3]+24, "%8x", &(aesIp->key[0]));

	//Write key
	for(i=0;i<4;i++)
	{
		printf("key%d: %x\n",i,aesIp->key[i]);
	}

	//Set mode to encrypt and load key
//	printf("ctrl\n");

//	printf("status: %x\n",aesIp->status);
	while(aesIp->status!=0x0);

	aesIp->ctrl=0x3;

	//Wait for subkey compute
//	printf("status: %x\n",aesIp->status);
	while(aesIp->status!=0x1);

	/* Step 1, open the DMA proxy device for the transmit and receive channels with
 	 * read/write permissions
 	 */

	tx_proxy_fd = open("/dev/dma_proxy_tx", O_RDWR);

	if (tx_proxy_fd < 1) {
		printf("Unable to open DMA proxy device file");
		return -1;
	}

	rx_proxy_fd = open("/dev/dma_proxy_rx", O_RDWR);
	if (tx_proxy_fd < 1) {
		printf("Unable to open DMA proxy device file");
		return -1;
	}

	/* Step 2, map the transmit and receive channels memory into user space so it's accessible
 	 */
	tx_proxy_interface_p = (struct dma_proxy_channel_interface *)mmap(NULL, sizeof(struct dma_proxy_channel_interface),
									PROT_READ | PROT_WRITE, MAP_SHARED, tx_proxy_fd, 0);

	rx_proxy_interface_p = (struct dma_proxy_channel_interface *)mmap(NULL, sizeof(struct dma_proxy_channel_interface),
									PROT_READ | PROT_WRITE, MAP_SHARED, rx_proxy_fd, 0);

	if ((rx_proxy_interface_p == MAP_FAILED) || (tx_proxy_interface_p == MAP_FAILED)) {
		printf("Failed to mmap\n");
		return -1;
	}

	//Input file
	input_fd= fopen(argv[1],"rb");
	if (input_fd==NULL)
	{
		perror("Error opening file for reading");
		exit(EXIT_FAILURE);
	}

	fseek(input_fd, 0L, SEEK_END);
	lSize = ftell(input_fd);
	printf("file size: %ld\n",lSize);
	if(lSize>8388576)
	{
		perror("File is too large");
		exit(EXIT_FAILURE);
	}
	rewind(input_fd);

	paddingSize=lSize%16;
	paddingSize=16-paddingSize;
	transferLength=lSize+paddingSize;

	tx_proxy_interface_p->length = transferLength;
	result = fread (tx_proxy_interface_p->buffer,1,lSize,input_fd);
	if (result != lSize)
	{
		perror("Error reading file");
		exit(EXIT_FAILURE);
	}
	//Add padding PKCS7
	for(i=0;i<paddingSize;i++)
	{
		tx_proxy_interface_p->buffer[lSize+i]=paddingSize;
	}
	fclose (input_fd);

	/* Create the thread for the transmit processing and then wait a second so the printf output is not
 	 * intermingled with the receive processing
	 */
	pthread_create(&tid, NULL, tx_thread, NULL);
//	sleep(1);

	/* Initialize the receive buffer so that it can be verified after the transfer is done
	 * and setup the size of the transfer for the receive channel
 	 */
	for (i = 0; i < 16; i++)
		rx_proxy_interface_p->buffer[i] = 0;

    rx_proxy_interface_p->length = transferLength;

	/* Step 3, Perform the DMA transfer and after it finishes check the status
	 */
    clock_gettime(CLOCK_REALTIME, &requestStart);
	ioctl(rx_proxy_fd, 0, &dummy);
	clock_gettime(CLOCK_REALTIME, &requestEnd);
	accum = ( requestEnd.tv_sec - requestStart.tv_sec )
	  + ( requestEnd.tv_nsec - requestStart.tv_nsec )
	  / BILLION;
	printf( "time: %lf\n", accum );

	if (rx_proxy_interface_p->status != PROXY_NO_ERROR)
		printf("Proxy rx transfer error\n");

	//Output file
	output_fd= fopen(argv[2], "wb");
	if (output_fd==NULL)
	{
		perror("Error opening file for writing");
		exit(EXIT_FAILURE);
	}
	fwrite (rx_proxy_interface_p->buffer , 1, transferLength, output_fd);
	fclose (output_fd);

	/* Unmap the proxy channel interface memory and close the device files before leaving
	 */
	munmap(tx_proxy_interface_p, sizeof(struct dma_proxy_channel_interface));
	munmap(rx_proxy_interface_p, sizeof(struct dma_proxy_channel_interface));

	close(tx_proxy_fd);
	close(rx_proxy_fd);

	if (munmap(aesIp, sizeof(AesIp_t)) == -1)
	{
		perror("Error un-mmapping the file");
	}

	close(aes_fd);
	return 0;
}
