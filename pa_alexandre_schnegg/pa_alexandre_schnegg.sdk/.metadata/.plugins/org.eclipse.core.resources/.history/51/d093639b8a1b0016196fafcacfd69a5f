#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include "dma_proxy.h"
#include <stdlib.h>
#include <string.h>

#define BILLION 1E9

static struct dma_proxy_channel_interface *tx_proxy_interface_p;
static int tx_proxy_fd;

typedef unsigned int uint32_t;

typedef struct
{
	uint32_t ctrl;
	uint32_t status;
	uint32_t key[4];
} AesIp_t;

//http://stackoverflow.com/questions/3408706/hexadecimal-string-to-byte-array-in-c
void hexStringToArray(const char hexstring[] , unsigned char val[],int size)
{
	const char *pos = hexstring;
    size_t count = 0;

     /* WARNING: no sanitization or error-checking whatsoever */
    for(count = 0; count < size; count++) {
        sscanf(pos, "%2hhx", &val[count]);
        pos += 2;
    }

    printf("0x");
    for(count = 0; count < size; count++)
        printf("%02x", val[count]);
    printf("\n");

}

/* The following function is the transmit thread to allow the transmit and the
 * receive channels to be operating simultaneously. The ioctl calls are blocking
 * such that a thread is needed.
 */
void *tx_thread()
{
	unsigned char plaintext[16];
	int dummy, i;

//	hexStringToArray("6bc1bee22e409f96e93d7e117393172a",plaintext,16);

	/* Perform the DMA transfer and the check the status after it completes
 	 * as the call blocks til the transfer is done.
 	 */
	ioctl(tx_proxy_fd, 0, &dummy);

	if (tx_proxy_interface_p->status != PROXY_NO_ERROR)
		printf("Proxy tx transfer error\n");
}

/* The following function uses the dma proxy device driver to perform DMA transfers
 * from user space. This app and the driver are tested with a system containing an
 * AXI DMA without scatter gather and with transmit looped back to receive.
 */
int main(int argc, char *argv[])
{
	struct dma_proxy_channel_interface *rx_proxy_interface_p;
	int rx_proxy_fd, i,aes_fd;
	int *input_fd,*output_fd;
	char *src, *dest;
	int dummy;
	pthread_t tid;
	long lSize;
	struct timespec requestStart, requestEnd;
	double accum;

	unsigned char ciphertext[16];
	unsigned char key[16];
	AesIp_t *aesIp;
	size_t result;

	printf("DMA proxy test\n");

	//Prepare test data
//	hexStringToArray("3ad77bb40d7a3660a89ecaf32466ef97",ciphertext,16);
//	hexStringToArray("2b7e151628aed2a6abf7158809cf4f3c",key);

	//Map the AES IP registers

	aes_fd = open("/dev/mem", O_RDWR|O_SYNC);
	if (aes_fd < 0)
	{
		perror("Error opening file for writing");
		exit(EXIT_FAILURE);
	}

	aesIp = (AesIp_t*) mmap(NULL, sizeof(AesIp_t), PROT_READ | PROT_WRITE, MAP_SHARED, aes_fd,
			0x43c00000);
	if (aesIp == MAP_FAILED)
	{
		close(aes_fd);
		perror("Error mmapping the file");
		exit(EXIT_FAILURE);
	}

	aesIp->key[0]=0x09cf4f3c;
	aesIp->key[1]=0xabf71588;
	aesIp->key[2]=0x28aed2a6;
	aesIp->key[3]=0x2b7e1516;


	//Write key
	for(i=0;i<4;i++)
	{
		printf("key%d: %x\n",i,aesIp->key[i]);
	}

	//Set mode to encrypt and load key
	aesIp->ctrl=0x0;
//	while(aesIp->status!=0x0);

	aesIp->ctrl=0x3;

	sleep(1);

	//Wait for subkey compute
	while(aesIp->status!=0x1);

	/* Step 1, open the DMA proxy device for the transmit and receive channels with
 	 * read/write permissions
 	 */

	tx_proxy_fd = open("/dev/dma_proxy_tx", O_RDWR);

	if (tx_proxy_fd < 1) {
		printf("Unable to open DMA proxy device file");
		return -1;
	}

	rx_proxy_fd = open("/dev/dma_proxy_rx", O_RDWR);
	if (tx_proxy_fd < 1) {
		printf("Unable to open DMA proxy device file");
		return -1;
	}

	/* Step 2, map the transmit and receive channels memory into user space so it's accessible
 	 */
	tx_proxy_interface_p = (struct dma_proxy_channel_interface *)mmap(NULL, sizeof(struct dma_proxy_channel_interface),
									PROT_READ | PROT_WRITE, MAP_SHARED, tx_proxy_fd, 0);

	rx_proxy_interface_p = (struct dma_proxy_channel_interface *)mmap(NULL, sizeof(struct dma_proxy_channel_interface),
									PROT_READ | PROT_WRITE, MAP_SHARED, rx_proxy_fd, 0);

	if ((rx_proxy_interface_p == MAP_FAILED) || (tx_proxy_interface_p == MAP_FAILED)) {
		printf("Failed to mmap\n");
		return -1;
	}

	//Input file
	input_fd=open("input",O_RDONLY);
	if (input_fd==NULL)
	{
		perror("Error opening file for reading");
		exit(EXIT_FAILURE);
	}
	lSize = lseek(input_fd, 0, SEEK_END);
	printf("file size: %ld\n",lSize);
	if(lSize>104857600)
	{
		perror("File is too large");
		exit(EXIT_FAILURE);
	}
	src = mmap(NULL, lSize, PROT_READ, MAP_PRIVATE, input_fd, 0);
	if (src == MAP_FAILED)
	{
		printf("Failed to mmap\n");
		return -1;
	}

	tx_proxy_interface_p->length = lSize;

	for(i=0;i<lSize;i++)
	{
		tx_proxy_interface_p->buffer[i]=src[lSize-i];
	}

	munmap(src, lSize);
	fclose (input_fd);


	/* Create the thread for the transmit processing and then wait a second so the printf output is not
 	 * intermingled with the receive processing
	 */
	pthread_create(&tid, NULL, tx_thread, NULL);
	sleep(1);

	/* Initialize the receive buffer so that it can be verified after the transfer is done
	 * and setup the size of the transfer for the receive channel
 	 */
	for (i = 0; i < 16; i++)
		rx_proxy_interface_p->buffer[i] = 0;

    rx_proxy_interface_p->length = lSize;

	/* Step 3, Perform the DMA transfer and after it finishes check the status
	 */
    clock_gettime(CLOCK_REALTIME, &requestStart);
	ioctl(rx_proxy_fd, 0, &dummy);
	clock_gettime(CLOCK_REALTIME, &requestEnd);
	accum = ( requestEnd.tv_sec - requestStart.tv_sec )
	  + ( requestEnd.tv_nsec - requestStart.tv_nsec )
	  / BILLION;
	printf( "time: %lf\n", accum );

	if (rx_proxy_interface_p->status != PROXY_NO_ERROR)
		printf("Proxy rx transfer error\n");

	/* Verify the data recieved matchs what was sent (tx is looped back to tx)
 	 */
//	for (i = 0; i < 16; i++) {
//        	if (ciphertext[15-i] !=
//            		rx_proxy_interface_p->buffer[i])
//            		printf("get: %x, expected: %x\n",rx_proxy_interface_p->buffer[i], ciphertext[15-i]);
//    	}

	//Output file
	output_fd=open("output", O_RDWR | O_CREAT, 0666);
	if (output_fd==NULL)
	{
		perror("Error opening file for writing");
		exit(EXIT_FAILURE);
	}
	ftruncate(output_fd, lSize);
	dest = mmap(NULL, lSize, PROT_READ | PROT_WRITE, MAP_SHARED, output_fd, 0);
	for(i=0;i<lSize;i++)
	{
		des[i]=rx_proxy_interface_p->buffer[lSize-i];
	}

	if (dest == MAP_FAILED)
	{
		printf("Failed to mmap\n");
		return -1;
	}

	munmap(dest, lSize);
	close (output_fd);

	/* Unmap the proxy channel interface memory and close the device files before leaving
	 */
	munmap(tx_proxy_interface_p, sizeof(struct dma_proxy_channel_interface));
	munmap(rx_proxy_interface_p, sizeof(struct dma_proxy_channel_interface));

	close(tx_proxy_fd);
	close(rx_proxy_fd);

	if (munmap(aesIp, sizeof(AesIp_t)) == -1)
	{
		perror("Error un-mmapping the file");
	}

	close(aes_fd);
	return 0;
}
